## 1. 가상 머신 사양

항목|사양|
| - | - |
운영체제 | Centos7
Cpu Core | 2
Memmory  | 4096MB
NIC | Nat Network

## 2. Requirement
### 1) Port
- Control Plane
```bash
sudo firewall-cmd --add-port 6443/tcp --permanent 2379-2380/tcp --permanent
```

```bash
sudo firewall-cmd --add-port 10250-10252/tcp --permanent
```

```bash
sudo firewall-cmd --reload
```

- Worker Node
```bash
sudo firewall-cmd --add-port 10250/tcp --permanent
```

```bash
sudo firewall-cmd --add-port 30000-32767/tcp --permanent
```

```bash
sudo firewall-cmd --reload
```

### 2) 모듈 설정 - All Node
- br_netfilter 모듈 로드
```bash
sudo modprobe br_netfilter
```

- 브릿지 커널 파라미터 수정
```bash
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
```

```bash
sudo sysctl --system
```

### 3) 스왑 비활성화 - All Node
- runtime swap off
```bash
sudo swapoff -a
```

- (Optional) Modify FileSystem Table
```bash
vi /etc/fstab
...
#/dev/mapper/centos-swap    swap    swap    defaults    0    0
```

### 4) 컨테이너 런타임 설치 - All Node
- 도커 설치
```bash
sudo yum install -y yum-utils
```

```bash
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
```

```bash
sudo yum install -y docker-ce docker-ce-cli containerd.io
```

```bash
sudo systemctl enable --now docker
```

```bash
sudo usermod -aG docker $USER
```

### 5) SELinux PErmissive 모드 전환
```bash
sudo setenforce 0
```

```bash
sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
```

## 3. Kubernetes 패키지 설치 - All Node
### 1) 저장소 연결
```bash
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF
```

### 2) 패키지 설치
```bash
sudo yum install -y kubelet-1.18.6-0 kubeadm-1.18.6-0 kubectl-1.18.6-0 --disableexcludes=kubernetes
```

```bash
sudo systemctl enable --now kubelet
```

## 4. 클러스터 생성 및 노드 추가
### 1) 클러스터 생성 - Control Plane
- CONTROL_PLANE_IP : Control Plane IP 지정
```bash
sudo kubeadm init --apiserver-advertise-address=192.168.56.11
```

> kubeadm init이 성공적으로 마무리되면 kubeadm join 명령을 잘 기록해둔다.

- 설정 파일 생성
```bash
mkdir -p $HOME/.kube
```

```bash
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
```

```bash
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

### 2) 워커 노드 추가 - Worker Node
- CONTROL_PLANE_IP : Control Plane IP 지정
```bash
sudo kubeadm join \
CONTROL_PLANE_IP:6443 \
--token rqo0ol.4rx4tqvkh68sfspb \
--discovery-token-ca-cert-hash sha256:340253bd1aa163db4780bc13ded83ec988c4958aff813fd0a36f58562659f4bc
```

> kubeadm join 명령은 kubeadm init이 성공적으로 끝났을 때 출력됨. 다시 확인하는 방법은 쿠버네티스 공식 문서에서 확인


### 3) Pod Network Plugin 설치
- Calico 설치 - Contorl Plane
```bash
curl https://docs.projectcalico.org/manifests/calico.yaml -O
```

```bash
kubectl apply -f calico.yaml
```

- BGP 포트 오픈 - All Node
```bash
sudo firewall-cmd --add-port=179/tcp --permanent
```

```bash
sudo firewall-cmd --add-port=179/tcp 
```
