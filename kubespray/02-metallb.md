
## 1. 준비

> kubectl edit configmap -n kube-system kube-proxy
```yaml
apiVersion: kubeproxy.config.k8s.io/v1alpha1
kind: KubeProxyConfiguration
mode: "ipvs"
ipvs:
  strictARP: true
```
해당 내용만 편집


## 2. 설치
> metal-lb 네임스페이스 생성
```bash
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.9.3/manifests/namespace.yaml
```
> metal-lb 디플로이먼트 및 데몬셋 생성
```bash
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.9.3/manifests/metallb.yaml
```

>??
```bash
kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)"
```

## 3. 구성 파일 생성
컨피그 맵을 생성해야 metallb 가 동작함

> vi config.yaml
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: metallb-system
  name: config
data:
  config: |
    address-pools:
    - name: default
      protocol: layer2
      addresses:
      - 192.168.1.240-192.168.1.250
```
```bash
$ kubectl apply -f config.yaml
```